let inventory= new Inventory()
let cart = new Cart()
let user = new User()
let selectedArticle = -1

const tableTbody2 = document.createElement('tbody')
const tableTbody = document.createElement('tbody')


let trSelected = null
let tr2Selected = null
document.addEventListener('DOMContentLoaded', () => {
  CreateInventory()
  SelecteArticleToInventory()
  SelecteArticleToPanier()
  AddArticleToPanier()
  RemoveArticleToPanier()
})

function CreateInventory(){
  const inventaire = inventory.articles
  inventaire.forEach((element, index) =>{
    const tableTr = document.createElement('tr')
    tableTr.setAttribute('data-id', index)
    const tableTd1 = document.createElement('td')
    let tableTd2 = document.createElement('td')
    const tableTd3 = document.createElement('td')
    
    table = document.getElementById('inventaire')
    tablepanier = document.getElementById('myPanier')
    
    table.appendChild(tableTbody)
    tablepanier.appendChild(tableTbody2)

    tableTbody.appendChild(tableTr)
    
    tableTr.appendChild(tableTd1)
    tableTr.appendChild(tableTd2)
    tableTr.appendChild(tableTd3)

    let name = document.createTextNode(element.name)
    let quantity= document.createTextNode(element.quantity)
    let price = document.createTextNode(element.price)

    tableTd1.appendChild(name)
    tableTd2.appendChild(quantity)
    tableTd3.appendChild(price)
  })
}
function SelecteArticleToInventory(){
  const inventaireTable = document.getElementById('inventaire')
  const inventaireTbody = inventaireTable.getElementsByTagName('tbody')
  inventaireTbody[0].addEventListener('click', (event) => {
    
    trSelected = event.target.parentNode
    if (trSelected.getAttribute("style")) {
      trSelected.removeAttribute("style")
    }
    else{
      trSelected.style.backgroundColor="aqua"
    }
    
    document.getElementById("addArticle").classList.toggle("disabled")
    document.getElementById("removeArticle").classList.toggle("disabled")
    console.log(trSelected)
  })
}

function SelecteArticleToPanier(){
  let panierTable = document.getElementById('myPanier')
  let panierTbody = panierTable.getElementsByTagName('tbody')
  panierTbody[0].addEventListener('click', (event) => {
    
    tr2Selected = event.target.parentNode
    
    if (tr2Selected.getAttribute("style")) {

      tr2Selected.removeAttribute("style")
    }
    else{
      tr2Selected.style.backgroundColor="aqua"
    }
    
    document.getElementById("removeArticle").classList.toggle("disabled")
    console.log(tr2Selected)
  })
}

function AddArticleToPanier(){
  let articlesPanier = document.getElementsByClassName("panierArticle")
  document.getElementById("addArticle").addEventListener('click', () =>{
    
    const index = trSelected.getAttribute('data-id')
    const element = inventory.articles[index]
    const tableTr2 = document.createElement('tr')
    const achatTd1 = document.createElement('td')
    const achatTd2 = document.createElement('td')
    
    tableTr2.classList.add('panierArticle')
    
    let name = document.createTextNode(element.name)
    let price = document.createTextNode(element.price)
    
    tableTbody2.appendChild(tableTr2)
    
    achatTd1.appendChild(name)
    achatTd2.appendChild(price)
    
    tableTr2.appendChild(achatTd1)
    tableTr2.appendChild(achatTd2)
    cart.addArticle(element)
    inventory.removeQuantityToArticle(element)
    
    let totalAchat = document.getElementById('total')
    let prix = document.createTextNode(cart.total+'€')
    totalAchat.innerHTML=prix.nodeValue
    
    console.log('toto',cart.articles);
    // console.log(cart);
    // console.log(articlesPanier)
  })
}


function RemoveArticleToPanier(){
  
  const inventaireTable2 = document.getElementById('myPanier')
  const inventaireTbody2 = inventaireTable2.getElementsByTagName('tbody')
  document.getElementById("removeArticle").addEventListener('click', () =>{
    for(i=0; i< cart.articles.length; i++){
      const element = cart.articles[i]
      cart.removeArticle(element)
      console.log('toto',cart.articles)
      let totalAchat = document.getElementById('total')
      let prix = document.createTextNode(cart.total+'€')
      totalAchat.innerHTML=prix.nodeValue
      
      inventaireTbody2[i].removeChild(tr2Selected)
      
      
    }
    
    
    
    
  })
}

function CheckBudget(){
  document.getElementById("removeArticle").addEventListener('click', () =>{


  })
}